<?php

namespace judahnator\Option\Tests;


use judahnator\Option\Drivers\MemoryDriver;
use judahnator\Option\OptionInterface;

class MemoryDriverTest extends DriverTestCase
{

    protected static function getDriver(): OptionInterface
    {
        return new MemoryDriver();
    }
}