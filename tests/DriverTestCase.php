<?php

namespace judahnator\Option\Tests;


use judahnator\Option\Option;
use judahnator\Option\OptionInterface;
use PHPUnit\Framework\TestCase;

abstract class DriverTestCase extends TestCase
{

    /** @var Option $option */
    private $option;
    
    abstract protected static function getDriver(): OptionInterface;

    protected function setUp()
    {
        $this->option = new Option(static::getDriver());
    }

    public function testDeleteMethod(): void
    {
        $this->option->set('foo', 'bar');
        $this->option->delete('foo');
        $this->assertFalse($this->option->has('foo'));
    }

    public function testGetMethod(): void
    {
        // Make sure null is returned if no default is set
        $this->assertNull($this->option->get('foo'));

        // Make sure the default value is returned
        $this->assertEquals('bar', $this->option->get('foo', 'bar'));
    }

    public function testHasMethod(): void
    {
        $this->option->set('foo', 'bar');
        $this->assertTrue($this->option->has('foo'));
    }

    public function testSetMethod(): void
    {
        $this->option->set('foo', 'bar');
        $this->assertEquals('bar', $this->option->get('foo'));
    }

}