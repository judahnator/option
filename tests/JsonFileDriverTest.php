<?php

namespace judahnator\Option\Tests;


use judahnator\Option\Drivers\JsonFileDriver;
use judahnator\Option\OptionInterface;

class JsonFileDriverTest extends DriverTestCase
{

    private static $optionsFile = __DIR__.'/options.json';

    protected function setUp()
    {
        file_put_contents(self::$optionsFile, json_encode([]));
        parent::setUp();
    }

    public static function tearDownAfterClass()
    {
        unlink(self::$optionsFile);
    }

    protected static function getDriver(): OptionInterface
    {
        return new JsonFileDriver(self::$optionsFile);
    }
}