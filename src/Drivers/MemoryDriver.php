<?php

namespace judahnator\Option\Drivers;


use judahnator\Option\OptionInterface;

class MemoryDriver implements OptionInterface
{

    protected $options = [];

    /**
     * Removes an option from the option keystore.
     *
     * @param string $key
     */
    public function delete(string $key): void
    {
        if ($this->has($key)) {
            unset($this->options[$key]);
        }
    }

    /**
     * Retrieve the option with a given key, or the $default if the option cannot be found.
     *
     * @param string $key
     * @param $default
     * @return mixed
     */
    public function get(string $key, $default = null)
    {
        if (!$this->has($key)) {
            return $default;
        }
        return $this->options[$key];
    }

    /**
     * Determine if the option keystore has a given $key.
     *
     * @param string $key
     * @return bool
     */
    public function has(string $key): bool
    {
        return array_key_exists($key, $this->options);
    }

    /**
     * Set a given option to a provided $value, overwriting existing data if necessary.
     *
     * @param string $key
     * @param $value
     */
    public function set(string $key, $value): void
    {
        $this->options[$key] = $value;
    }
}