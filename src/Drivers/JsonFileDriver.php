<?php

namespace judahnator\Option\Drivers;


use judahnator\Option\OptionInterface;

class JsonFileDriver extends MemoryDriver implements OptionInterface
{

    private $optionsFile;

    public function __construct(string $optionsFile)
    {
        if (!file_exists($optionsFile)) {
            throw new \LogicException('Cannot find the options file');
        }
        $this->optionsFile = $optionsFile;
        $this->options = json_decode(file_get_contents($this->optionsFile), true);
    }

    public function __destruct()
    {
        // This check is in place because the tests will try to delete the file, then this will just put it back
        if (file_exists($this->optionsFile)) {
            file_put_contents($this->optionsFile, json_encode($this->options, JSON_PRETTY_PRINT));
        }
    }

}