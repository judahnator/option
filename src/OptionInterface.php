<?php

namespace judahnator\Option;

interface OptionInterface
{

    /**
     * Removes an option from the option keystore.
     *
     * @param string $key
     */
    public function delete(string $key): void;

    /**
     * Retrieve the option with a given key, or the $default if the option cannot be found.
     *
     * @param string $key
     * @param $default
     * @return mixed
     */
    public function get(string $key, $default = null);

    /**
     * Determine if the option keystore has a given $key.
     *
     * @param string $key
     * @return bool
     */
    public function has(string $key): bool;

    /**
     * Set a given option to a provided $value, overwriting existing data if necessary.
     *
     * @param string $key
     * @param $value
     */
    public function set(string $key, $value): void;

}