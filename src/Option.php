<?php

namespace judahnator\Option;

class Option implements OptionInterface
{

    protected $driver;

    public function __construct(OptionInterface $driver)
    {
        $this->driver = $driver;
    }

    /**
     * Removes an option from the option keystore.
     *
     * @param string $key
     */
    public function delete(string $key): void
    {
        $this->driver->delete($key);
    }

    /**
     * Retrieve the option with a given key, or the $default if the option cannot be found.
     *
     * @param string $key
     * @param $default
     * @return mixed
     */
    public function get(string $key, $default = null)
    {
        return $this->driver->get($key, $default);
    }

    /**
     * Determine if the option keystore has a given $key.
     *
     * @param string $key
     * @return bool
     */
    public function has(string $key): bool
    {
        return $this->driver->has($key);
    }

    /**
     * Set a given option to a provided $value, overwriting existing data if necessary.
     *
     * @param string $key
     * @param $value
     */
    public function set(string $key, $value): void
    {
        $this->driver->set($key, $value);
    }
}