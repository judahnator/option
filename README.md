PHP Options Package
===================

This is a relatively straightforward library for storing and retrieving
various "options" in your application. This is great for stuff
that is not appropriate for environment variables, but that might
not need an entire application model.

Installation
------------

You can install this package via composer.

`composer require judahnator/option`

Usage
-----

First thing you need to do is pass what driver you plan to use
into the Option class constructor.

For debugging and demonstration purposes, we will use the 
`MemoryDriver` provided with this library. You can also write
your own drivers, there is a section for that later in this readme.

```php
<?php

use judahnator\Option\Drivers\MemoryDriver;
use judahnator\Option\Option;

$option = new Option(new MemoryDriver());
```

* To get an option:
  * `$option->get('foo');`
* To check if an option exists:
  * `$option->has('foo');`
* To set (or overwrite) an option:
  * `$option->set('foo', 'foos value')`
* To delete an option:
  * `$option->delete('foo')`

Drivers
-------

There are two drivers provided by default with this library.

* `\judahnator\Option\Drivers\JsonFileDriver`
* `\judahnator\Option\Drivers\MemoryDriver`

The first accepts an argument for where you want your options stored.
The later is purely in-memory, and is best used for only debugging
or demonstration purposes.

Creating your own Option driver is simple as well. Create a new class
that extends the `\judahnator\Option\OptionInterface` interface.
You can now use that as a driver for the Option class.